package com.example.mrhead

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView

class MainActivity : AppCompatActivity() {

    // Mendeklarasikan Variabel
    private lateinit var rambut: ImageView
    private lateinit var alis: ImageView
    private lateinit var Jenggot: ImageView
    private lateinit var kumis: ImageView
    private lateinit var cekBoxRambut: CheckBox
    private lateinit var cekBoxAlis: CheckBox
    private lateinit var cekBoxJenggot: CheckBox
    private lateinit var cekBoxKumis: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Inisial Variable
        rambut = findViewById(R.id.hair)
        alis   = findViewById(R.id.eyebrow)
        Jenggot= findViewById(R.id.beard)
        kumis  = findViewById(R.id.moustache)
        cekBoxRambut = findViewById(R.id.hairCheck)
        cekBoxAlis   = findViewById(R.id.eyebrowCheck)
        cekBoxJenggot= findViewById(R.id.eyebrowBeard)
        cekBoxKumis  = findViewById(R.id.moustacheCheck)

        //Mengaplikasikan checkbox
        cekBoxRambut.setOnClickListener {
            rambut.visibility = if (cekBoxRambut.isChecked) View.VISIBLE else View.GONE
        }

        cekBoxAlis.setOnClickListener {
            alis.visibility = if (cekBoxAlis.isChecked) View.VISIBLE else View.GONE
        }

        cekBoxJenggot.setOnClickListener {
            Jenggot.visibility = if (cekBoxJenggot.isChecked) View.VISIBLE else View.GONE
        }

        cekBoxKumis.setOnClickListener {
            kumis.visibility = if (cekBoxKumis.isChecked) View.VISIBLE else View.GONE
        }
    }
}